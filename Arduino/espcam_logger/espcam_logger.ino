#include "esp_camera.h"
#include "Arduino.h"
#include "FS.h"                // SD Card ESP32
#include "SD_MMC.h"            // SD Card ESP32
#include "soc/soc.h"           // Disable brownour problems
#include "soc/rtc_cntl_reg.h"  // Disable brownour problems
#include "driver/rtc_io.h"
#include <EEPROM.h>            // read and write from flash memory
#include <Wire.h>

// Pin definition for CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22
//--------------------- Struct --> json -----------------------------
#include <ArduinoJson.h>

StaticJsonDocument<512> json;

struct Vector_3D_t{
	int16_t x;
	int16_t y;
	int16_t z;
};


struct sdoc_t{
	struct{
		uint16_t temp;
		uint16_t pressure;
		uint16_t altitude;
	}bmp;
	struct{
		uint8_t humidity;
		uint8_t temp;
	}dht;
	uint16_t mq7;
	struct{
		struct Vector_3D_t accelerometer;
		struct Vector_3D_t gyroscope;
		uint8_t temp;
	}mpu;
} sdoc;

int pictureNumber=0;
int newMessage=0;
String folder;
String logPath;
//--------------------------------------------------

void setup(){
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
 
  Serial.begin(115200);
	pinMode(33, OUTPUT);
	digitalWrite(33,HIGH);
  //Serial.setDebugOutput(true);
  //Serial.println();
  
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG; 
  
  if(psramFound()){
    config.frame_size = FRAMESIZE_UXGA; // FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA
    config.jpeg_quality = 6;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }
  
  // Init Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
		digitalWrite(33,LOW);
    return;
  }
  
  //Serial.println("Starting SD Card");
  if(!SD_MMC.begin()){
    Serial.println("SD Card Mount Failed");
		digitalWrite(33,LOW);
    return;
  }
  
  uint8_t cardType = SD_MMC.cardType();
  if(cardType == CARD_NONE){
    Serial.println("No SD Card attached");
		digitalWrite(33,LOW);
    return;
  }
	
	folder = createLogDir(SD_MMC);
	logPath = folder + "/msgLog.txt";
	writeFile(SD_MMC, logPath.c_str() , "");

	Wire.begin(8,1,3,0); //adress,sda, scl, frequency
	Wire.onReceive(recieveEvent);

	/* delay(1000); */
	/* takePhoto(); */
}

void recieveEvent(int howMany){
		Serial.println("Message recieved");
		Wire.readBytes((byte*) &sdoc, sizeof sdoc);
		saveJson();
		takePhoto();
		/* newMessage++; */
		pictureNumber++;
		Serial.println("new message");
}
void loop(){
	/* if(newMessage > 0){ */
	/* 	takePhoto(); */
  
	/* 	digitalWrite(33, LOW); */
	/* 	/\* delay(2000); *\/ */
	/* 	int acTime=millis(); */
	/* 	while(millis() < acTime + 1000){ */
  /*       //wait approx. [period] ms */
  /*   } */
	/* 	digitalWrite(33, HIGH); */
	/* 	pictureNumber++; */
	/* 	newMessage=0; */
	/* } */

}

void takePhoto(){
		camera_fb_t * fb = NULL;
  
		// Take Picture with Camera
		fb = esp_camera_fb_get();  
		if(!fb) {
			Serial.println("Camera capture failed");
			digitalWrite(33,LOW);
			return;
		}
		// initialize EEPROM with predefined size
		/* EEPROM.begin(EEPROM_SIZE); */
		/* pictureNumber = EEPROM.read(0) + 1; */

		// Path where new picture will be saved in SD Card
		String path = folder + "/picture" + String(pictureNumber) +".jpg";

		fs::FS &fs = SD_MMC; 
		Serial.printf("Picture file name: %s\n", path.c_str());
  
		File file = fs.open(path.c_str(), FILE_WRITE);
		if(!file){
			Serial.println("Failed to open file in writing mode");
			digitalWrite(33,LOW);
		} 
		else {
			file.write(fb->buf, fb->len); // payload (image), payload length
			Serial.printf("Saved file to path: %s\n", path.c_str());
		}
		file.close();
		esp_camera_fb_return(fb); 
}

void saveJson(){
	
			json["bmp"]["temp"]								= sdoc.bmp.temp/100.0;
			json["bmp"]["pressure"]						= sdoc.bmp.pressure;
			json["bmp"]["altitude"]						= sdoc.bmp.altitude/10.0;
			json["dht"]["humidity"]						= sdoc.dht.humidity;
			json["dht"]["temp"]								= sdoc.dht.temp;
			json["mq7"]												= sdoc.mq7;
			json["mpu"]["accelerometer"]["x"] = sdoc.mpu.accelerometer.x /100.0;
			json["mpu"]["accelerometer"]["y"] = sdoc.mpu.accelerometer.y /100.0;
			json["mpu"]["accelerometer"]["z"] = sdoc.mpu.accelerometer.z /100.0;
			json["mpu"]["gyroscope"]["x"]			= sdoc.mpu.gyroscope.x /100.0;
			json["mpu"]["gyroscope"]["y"]			= sdoc.mpu.gyroscope.y /100.0;
			json["mpu"]["gyroscope"]["z"]			= sdoc.mpu.gyroscope.z /100.0;
			json["mpu"]["temp"]								= sdoc.mpu.temp;
			json["pictureNumber"]							= pictureNumber;

			String jsonString;
			serializeJson(json, jsonString);
			
			fs::FS &fs = SD_MMC;
			appendFile(fs, logPath.c_str(), jsonString.c_str());
}




void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.println(message)){
        Serial.println("Message appended");
    } else {
        Serial.println("Append failed");
    }
}

void createDir(fs::FS &fs, const char * path){
  Serial.printf("Creating Dir: %s\n", path);
  if(fs.mkdir(path)){
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

String createLogDir(fs::FS &fs){
	int i=0;
	while(true){ //Be careful, an error here can stop everything
		String path = "/log" + String(i);
		if(fs.exists(path) == 0){
			createDir(fs, path.c_str());
			return path;
		}
		i++;
	}
}
