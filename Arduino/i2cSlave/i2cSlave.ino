/* #include <Wire.h> */

/* #define SLAVE_ADDRESS 8 */

/* void setup() { */
/*   Wire.begin(13, 15, SLAVE_ADDRESS); // Initialize I2C communication on alternative pins */
/*   Wire.onReceive(receiveEvent); // Set up receive event */
/*   Serial.begin(115200); */
/*   Serial.println("Slave device initialized"); */
/* } */

/* void loop() { */
/*   delay(100); */
/* } */

/* void receiveEvent(int bytesReceived) { */
/*   Serial.print("Received data: "); */
/*   while (Wire.available()) { // Read all characters */
/*     char c = Wire.read(); // Read a single character */
/*     Serial.print(c); // Print received character */
/*   } */
/*   Serial.println(); */
/* } */


#include <Wire.h>
 
byte RxByte;
 
void I2C_RxHandler(int numBytes)
{
	Serial.println("Recieved");
	digitalWrite(4,HIGH);
  while(Wire.available()) {  // Read Any Received Data
    RxByte = Wire.read();
  }
	Serial.println(RxByte);
}
 
void setup() {
	Serial.begin(115200);
	pinMode(4,OUTPUT);
  Wire.begin(8,1,3,0); // Initialize I2C (Slave Mode: address=0x55 )
  Wire.onReceive(I2C_RxHandler);
	Serial.println("Started");
}
 
void loop() {
  // Nothing To Be Done Here
}
