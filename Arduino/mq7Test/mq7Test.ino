 // Source

/* MQ-7 Carbon Monoxide Sensor Circuit with Arduino */
const int AOUTpin = 32; //the AOUT pin of the CO sensor goes into analog pin A0 of the arduino
int lastCycle=0;
bool mq7Heating = false;

void setup() {
  Serial.begin(115200);//sets the baud rate
	lastCycle=millis();
	pinMode(33, OUTPUT);
}


void loop()
{
	if(mq7Heating == true){
		if((lastCycle + 10000) < millis()){
			lastCycle = millis();
			mq7Heating=false; 
			digitalWrite(33,LOW);
			Serial.println("mq7 OFF");
			Serial.println(mq7Heating);
		}
	}
	else{
		if((lastCycle + 10000) < millis()){
			lastCycle = millis();
			mq7Heating=true;
			digitalWrite(33,HIGH);
			Serial.println("mq7 ON");
			Serial.println(mq7Heating);
		}
	}
  Serial.print("CO value: ");
  Serial.println(analogRead(AOUTpin));//prints the CO value
  delay(100);
}

