/* 
  Receive the lora signal to control the on and off of the LED.
*/

#include "heltec.h"
#include <ArduinoJson.h>
#include "string.h"
#include "stdio.h"
#define LED 25
#define BAND    868E6  //you can set band here directly,e.g. 868E6,915E6

StaticJsonDocument<512> json;

struct Vector_3D_t{
	int16_t x;
	int16_t y;
	int16_t z;
};


struct sdoc_t{
	struct{
		uint16_t temp;
		uint16_t pressure;
		uint16_t altitude;
	}bmp;
	struct{
		uint8_t humidity;
		uint8_t temp;
	}dht;
	uint16_t mq7;
	struct{
		struct Vector_3D_t accelerometer;
		struct Vector_3D_t gyroscope;
		uint8_t temp;
	}mpu;
} sdoc;

void setup() {
  Heltec.begin(false/*DisplayEnable Enable*/, true /*Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
	pinMode(LED,OUTPUT);
  digitalWrite(LED,LOW); 
  Serial.begin(115200);
}
void loop() {
	  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    //Serial.print("Received packet '");
    // read packet
		LoRa.readBytes((byte*)&sdoc, packetSize);
    // print RSSI of packet
    /* Serial.print(" with RSSI "); */
		json["rssi"]											= LoRa.packetRssi();
		json["bmp"]["temp"]								= sdoc.bmp.temp/100.0;
		json["bmp"]["pressure"]						= sdoc.bmp.pressure;
		json["bmp"]["altitude"]						= sdoc.bmp.altitude/10.0;
		json["dht"]["humidity"]						= sdoc.dht.humidity;
		json["dht"]["temp"]								= sdoc.dht.temp;
		json["mq7"]												= sdoc.mq7;
		json["mpu"]["accelerometer"]["x"] = sdoc.mpu.accelerometer.x /100.0;
		json["mpu"]["accelerometer"]["y"] = sdoc.mpu.accelerometer.y /100.0;
		json["mpu"]["accelerometer"]["z"] = sdoc.mpu.accelerometer.z /100.0;
		json["mpu"]["gyroscope"]["x"]			= sdoc.mpu.gyroscope.x /100.0;
		json["mpu"]["gyroscope"]["y"]			= sdoc.mpu.gyroscope.y /100.0;
		json["mpu"]["gyroscope"]["z"]			= sdoc.mpu.gyroscope.z /100.0;
		json["mpu"]["temp"]								= sdoc.mpu.temp;


		String jsonString;
		serializeJson(json, jsonString);
		Serial.println(jsonString);
  }
}
