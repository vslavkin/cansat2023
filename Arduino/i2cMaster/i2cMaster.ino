#include <Wire.h>

void setup() {
  Wire.begin(17, 13); // Initialize I2C communication on pins 17 and 13
  Serial.begin(115200);
}

void loop() {
	Serial.println("Writing to i2c");

  Wire.beginTransmission(8); // Transmit to slave device with address 8
  Wire.write(1); // Send a single character
  Wire.endTransmission(); // End transmission
  delay(100);
}
