  /* 
    Send a lora signal to control the LED light switch of another board.
  */
#include "heltec.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP085.h>
#include <DHT.h>
#include <Wire.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Adafruit_MPU6050.h>

//----------------- Lora -----------------
#define BAND    868E6  //you can set band here directly,e.g. 868E6,915E6

//---------------- BMP180 -------------------
Adafruit_BMP085 bmp; //lib class for connecting to bmp180
#define i2c_sda 17  
#define i2c_scl 13
TwoWire i2cInstance = TwoWire(0); //i2c instance for bmp180

//---------------- DHT11 -------------------
#define DHTPIN 23
#define DHTTYPE DHT11
DHT dht = DHT(DHTPIN, DHTTYPE);

//--------------- MQ7 ----------------------
#define mq7Pin 32
#define mq7Power 33
int lastCycle=0;
bool mq7Heating = true;

//---------------neo6m*GPS*--------------
#define RXPin 1
#define TXPin 3
TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);

//--------------mpu6050-------------------
Adafruit_MPU6050 mpu;
Adafruit_Sensor *mpu_temp, *mpu_accel, *mpu_gyro;

//-------------- STRUCT ----------------------

struct Vector_3D_t{
	int16_t x;
	int16_t y;
	int16_t z;
};

struct sdoc_t{
	struct{
		uint16_t temp;
		uint16_t pressure;
		uint16_t altitude;
	}bmp;
	struct{
		uint8_t humidity;
		uint8_t temp;
	}dht;
	uint16_t mq7;
	struct{
		struct Vector_3D_t accelerometer;
		struct Vector_3D_t gyroscope;
		uint8_t temp;
	}mpu;
}sdoc; 


void setup() {
	Serial.begin(115200);

	Heltec.begin(false /*DisplayEnable Enable*/, true  /*Heltec.LoRa Disable*/, false /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
	LoRa.setTxPower(14,RF_PACONFIG_PASELECT_PABOOST);

  i2cInstance.begin(i2c_sda, i2c_scl); //i2c instance for bmp180

	if (!bmp.begin(0,&i2cInstance)) {
		Serial.println("Could not find a valid BMP085 sensor, check wiring!");
  }

	dht.begin();

	if(!mpu.begin(0x68, &i2cInstance)){
		Serial.println("mpu not working");
	}
	
	
	mpu_temp = mpu.getTemperatureSensor();
  mpu_temp->printSensorDetails();
	
  mpu_accel = mpu.getAccelerometerSensor();
  mpu_accel->printSensorDetails();

  mpu_gyro = mpu.getGyroSensor();
  mpu_gyro->printSensorDetails();


  /* ss.begin(9600); */

	pinMode(mq7Power,OUTPUT);
	digitalWrite(mq7Power, HIGH);
}
  
void loop() {
	mq7Controller();

	Serial.println("Reading sensor begins");
	/* while(ss.available() > 0) */
	/* 	gps.encode(ss.read()); */
	/* doc["gps"]["lat"] = gps.location.lat(); */
	/* doc["gps"]["long"] = gps.location.lng(); */


  sensors_event_t accel;
  sensors_event_t gyro;
  sensors_event_t temp;
  mpu_temp->getEvent(&temp);
  mpu_accel->getEvent(&accel);
  mpu_gyro->getEvent(&gyro);

	//Updating struct values
	sdoc = {	
		.bmp = {
			.temp			= bmp.readTemperature() * 100.0,	//bmp.readTemperature();
			.pressure	= bmp.readPressure(),
			.altitude	= bmp.readAltitude() * 10.0,
		},
		.dht = {
			.humidity	= dht.readHumidity(),
			.temp			= dht.readTemperature(),
		},
		.mq7 = analogRead(mq7Pin),
		.mpu = {
			.accelerometer = {
				.x = accel.acceleration.x * 100.0,
				.y = accel.acceleration.y * 100.0,
				.z = accel.acceleration.z * 100.0,
			},
			.gyroscope = {
				.x = gyro.gyro.x * 100.0,
				.y = gyro.gyro.y * 100.0,
				.z = gyro.gyro.z * 100.0,
			},
			.temp	= temp.temperature ,
		}
	};

	Serial.println("Reading sensors ends");
	LoRa.beginPacket();
	Serial.println("LoRa ends");
	LoRa.write((byte*)&sdoc, sizeof(sdoc));
	Serial.println("LoRa begins");
	LoRa.endPacket(true); //Argument as true so its asyncronic, the program continues 

	Serial.println("Sending i2c");
	i2cInstance.beginTransmission(8); // Transmit to device number 4
	i2cInstance.write((byte *)&sdoc, sizeof(sdoc_t)); //String -> *char
	i2cInstance.endTransmission();      // Stop transmitting
	Serial.println("i2c Sent");
	
	/* delay(1000);   */  //No delay, already too slow
}

void mq7Controller(){
	if(mq7Heating == true){
		if((lastCycle + 50000) <= millis()){
			lastCycle = millis();
			mq7Heating=false; 
			digitalWrite(mq7Power,LOW);
			Serial.println("mq7 OFF");
			Serial.println(mq7Heating);
		}
	}
	else{
		if((lastCycle + 90000) <= millis()){
			lastCycle = millis();
			mq7Heating=true;
			digitalWrite(mq7Power,HIGH);
			Serial.println("mq7 ON");
			Serial.println(mq7Heating);
		}
	}
}
