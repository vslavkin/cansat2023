docker run -d -p 3000:3000 --net=host --name=grafana -e "GF_INSTALL_PLUGINS=redis-datasource" -v grafana-volume:/var/lib/grafana grafana/grafana
docker run --name=redis -p 6379:6379 -v redis-volume:/data -it redis/redis-stack-server
docker run -v redisinsight:/db -p 8001:8001 redislabs/redisinsight:latest
