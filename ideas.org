* Porta pilas
Orejas + pines
Traba para la bateria 
1,2 mm de espesor
Conector horizontal del lado bottom
* Camara
Placa a 90 grados
Todos los pines de la camara de un lado
Pines tensores si o si
* Placa base
Dejar margen placa cortar al final calibrando con la tapa rosca
* Antena
Placa doble faz a 90 con ranura en placa base en masa gnd
Contemplar 2 pines tensores para placa prototipo, en tarjeta final no hace falta
* GPS
Soldar antena?
[[https://www.rcgroups.com/forums/showthread.php?2093050-NEO-6M-Solder-antenna-or-not-%21][Parece que si]]
* Peso
| Componente               | Peso/masa [g] | Peso reducido [g] |
|--------------------------+---------------+-------------------|
| Componentes electronicos |          55,5 |              55,5 |
| Paracaidas               |            50 |                30 |
| Placa                    |            30 |                30 |
| Estructura               |            90 |                70 |
| Bateria                  |            50 |                20 |
|--------------------------+---------------+-------------------|
| Total                    |           280 |               210 |
| Saldo                    |           -40 |                30 |
#+TBLFM: @7$2=vsum(@2..@6)::@7$3=vsum(@2..@6)::@8$2=(240-@7)::@8$3=(240-@7)
