import serial
import redis
import time 
import json

# Configure the serial port
port=input("Write the lora_Rx port \n")
# port = '/dev/ttyUSB0' 
baudrate = 115200
# Open the serial port
ser = serial.Serial(port, baudrate)

# Configure redis and redis TimeSeries
r = redis.Redis(host='localhost', port=6379, decode_responses=True)
ts = r.ts() 

def print_json(json_obj, fatherkey=''):
    if isinstance(json_obj, dict): #Si hay un json dentro del json
        for key, value in json_obj.items(): #Leer todos los keys del json
            print_json(value, fatherkey=f"{fatherkey}:{key}" if fatherkey else key)
    else:
        ts.add(f"{fatherkey}", "*", json_obj) #Agregar el key con su value a redis
        print(f"{fatherkey} = {json_obj}")

while True:
    if ser.in_waiting > 0:
        print("Serial recieved")
        line = ser.readline().decode('utf-8').rstrip()  # Read a line of data
        print(line)
        json_obj = json.loads(line)
        # jsonToRedis(json_obj)
        print_json(json_obj)
        # print(json_obj)  # Print the received data
    time.sleep(0.001)


